### Radar chart / Spyder graph

Some modifications (adaptating it for d3 v4, adding support for legend) around the code from ["A different look for the D3 radar chart"](http://www.visualcinnamon.com/2015/10/different-look-d3-radar-chart.html).